import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
public class Main2 {
	static ArrayList<String> li = new ArrayList<>();

	final static int ADD = 1;
	final static int UPDATE=2;
	final static int SEARCH=3;
	final static int DELETE=4;
	final static int EXIT=5;

	public static void loadProductList() throws IOException {

		String filePath = "C:\\Users\\SAGAR\\eclipse-workspace\\ProductManagement\\src\\product";
		File file = new File(filePath);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		while ((line = br.readLine()) != null) {
			li.add(line);
		}
	}

	public static void main(String[] args) throws Exception {

		
		System.out.println("* WELCOME TO PRODUCT MANAGEMENT *");
		
		loadProductList();

		int optionNumber = 0;
		Scanner sc = new Scanner(System.in);

		while (optionNumber != 5) {
			showOption();
			optionNumber = 0;
			System.out.println("Enter Your Choice :\n\n");
			optionNumber = sc.nextInt();

			if (optionNumber == ADD) {
				addProduct();
			} else if (optionNumber == UPDATE) {
				updateProduct();

			} else if (optionNumber == SEARCH) {
				searchProduct();
			} else if (optionNumber == DELETE) {
				deleteProduct();
			} else if (optionNumber == EXIT) {
				exit();
			} else {
				System.out.println("Invalid option");
			}
		}
	}

	
	public static void exit() throws Exception
	{
		writeArrayListToFile();
		System.exit(0);
	}
	
	public static void showOption() {
		System.out.println("Choose your option :");
		System.out.println("\t 1.Add Product");
		System.out.println("\t 2.Update product");
		System.out.println("\t 3.Search Product");
		System.out.println("\t 4.Delete Product");
		System.out.println("\t 5.Exit");

	}

	public static void addProduct() throws IOException {
		System.out.println("* Add Product *");
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Product :");
		String productName = sc.nextLine();
		System.out.println("Enter the Price :");
		double price = sc.nextInt();
		System.out.println("Enter Product Quantity :");
		int quantity = sc.nextInt();
		li.add(productName + "," + price + "," + quantity + "\n");
	}

	public static void updateProduct() throws IOException {
		System.out.println("* Update Product *\n\n");
		System.out.println("** Enter Product Name Which You Want to Update:");
		Scanner sc = new Scanner(System.in);
		String productNameToUpdate = sc.nextLine();

		System.out.println("You are Updating the Product:" + productNameToUpdate);
		System.out.println("Enter the Product Name:");
		String pName = sc.nextLine();
		System.out.println("Enter the perQuantityPrice:");
		double qprice = sc.nextInt();
		System.out.println("Enter the Product Quantity:");
		int pquantity = sc.nextInt();
		li.set(getIndexOf(productNameToUpdate), pName + "," + qprice + "," + pquantity);

	}
	
	public static void deleteProduct() throws IOException {
		System.out.println("* Delete Product *\n\n");
		System.out.println("** Enter Product Name Which You Want to Delete:");
		Scanner sc = new Scanner(System.in);
		String productNameToDelete = sc.nextLine();

		li.remove(getIndexOf(productNameToDelete));

	}
	public static void searchProduct() {
		System.out.println("* Search Product *\n\n");
		System.out.println("** Enter Product Name Which You Want to Search:");
		Scanner sc = new Scanner(System.in);
		String productNameToSearch = sc.nextLine();
		
		li.get(getIndexOf(productNameToSearch));
		System.out.println(productNameToSearch);
	}
	public static int getIndexOf(String itemName) {
		int i = 0;
		for (String s : li) {
			if (s.contains(itemName)) {
				return i;
			}
			i = i + 1;
		}
		return -1;
		
	}
	public static void writeArrayListToFile() throws Exception {
		File file = new File("C:\\Users\\SAGAR\\eclipse-workspace\\ProductManagement\\src\\product");
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		for (String s : li) {
			bw.write(s);
		}
		bw.close();
		fw.close();
	}
}
