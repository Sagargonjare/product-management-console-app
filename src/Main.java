import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {
	static Product product;
	public static void main(String[] args) throws IOException {
		int optionNumber=0;
			
		System.out.println("***** Welcome to Product Management System ****\n\n");
		
		while(optionNumber!=5)
		{
			showOptions();
			System.out.print("\nEnter your option :");
			Scanner sc = new Scanner(System.in);	
			optionNumber = sc.nextInt();
			
			if(optionNumber==1)
			{
				addProduct();
			}else if(optionNumber==2)
			{
				updateProduct();
			}else if(optionNumber==3)
			{
				
			}else if(optionNumber==4)
			{
				deleteProduct();
			}else if(optionNumber==5)
			{
				System.out.println("\n**** Exiting application");
			}else {
				System.out.println("Invalid option");
			}
			
		}
	}
		
		public static void showOptions()
		{
			System.out.println("Choose Operations : " );
			System.out.println("\t1. Add Product");
			System.out.println("\t2. Update Product");
			System.out.println("\t3. Search Product");
			System.out.println("\t4. Delete Product");
			System.out.println("\t5. Exit");

		}
		
		public static void addProduct()
		{
			System.out.println("**** Add Product ***\n\n");
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Product Name    : ");
			String productName = sc.nextLine();
			System.out.print("\nProduct Price  : ");
			double price = sc.nextDouble();
			System.out.print("\nProduct Quantity: ");
			int quantity = sc.nextInt();
		
			product = new Product(productName, price, quantity);
			
			writeProductToFile(product);
		}
		public static void updateProduct()
		{
			System.out.println("**** Add Product ***\n\n");
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Product Name    : ");
			String productName = sc.nextLine();
			System.out.print("\nProduct Price  : ");
			double price = sc.nextDouble();
			System.out.print("\nProduct Quantity: ");
			int quantity = sc.nextInt();
		
			product = new Product(productName, price, quantity);
			
			writeProductToFile(product);
		}
		public static void updateProductToFile(Product argProduct) {
			
			File file = new File("C:\\Users\\SAGAR\\eclipse-workspace\\ProductManagement\\src\\product");

		    try {
				FileWriter fw = new FileWriter(file); 
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(argProduct.productName+","+argProduct.perProductPrice+","+argProduct.productQuantity+"\n");
				bw.close();	
			    fw.close();

			} catch (IOException e) {
				
				e.printStackTrace();
			}	
		}
		public static void deleteProduct() throws IOException 
		{
			Scanner sc = new Scanner(System.in);
			
			System.out.println("Enter the product to delete");
			String product= sc.nextLine();
			
			String filepath="C:\\Users\\SAGAR\\eclipse-workspace\\ProductManagement\\src\\product";
			File file = new File(filepath);
			FileReader  fr = new FileReader(file);
			BufferedReader br= new BufferedReader (fr); 
			String line = "";
			
			String filepath1="C:\\Users\\SAGAR\\eclipse-workspace\\ProductManagement\\src\\update";
			File file1 = new File(filepath);
			FileWriter  fw = new FileWriter(file);
			BufferedWriter bw= new BufferedWriter (fw); 
			while (line !=null) {
				line = br.readLine();
				if ((line !=null)&&(!line.contains (product))){ 
					bw.write("\n");
				}
			}
			
			fw.close();
			bw.close();
			fr.close();
			br.close();
		}

		private static void writeProductToFile(Product argProduct) {
			
			File file = new File("C:\\Users\\SAGAR\\eclipse-workspace\\ProductManagement\\src\\product");

		    try {
				FileWriter fw = new FileWriter(file,true); 
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(argProduct.productName+","+argProduct.perProductPrice+","+argProduct.productQuantity+"\n");
				bw.close();	
			    fw.close();

			} catch (IOException e) {
				
				e.printStackTrace();
			}		    
		}

}
